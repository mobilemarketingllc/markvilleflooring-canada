<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer data-name="<?php echo get_the_title($_GET['id']);?>" class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>

<?php global $post; 

if($post->ID == '1054196'){
?>
<script>

function firstLoad(){
    
	if(jQuery('.product_color_select2')){
		var selected = jQuery('.product_color_select2 select');	
		var options = jQuery('.product_color_select2 select option');
		document.write('<div id="currentRugValue" style="display: none;"><?php echo get_the_title($_GET['id']);?></div>');
		var currentValue = document.getElementById('currentRugValue').innerHTML;
		options.each(function(){
			if(jQuery(this).attr('value') ===  currentValue){
				selected.val(currentValue);					
			}
		});
	}
	
}

firstLoad();

</script>
<?php } ?>
</body>
</html>
