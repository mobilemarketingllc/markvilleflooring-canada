<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

 

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

 


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



remove_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

// //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_two' );

function wpse_100012_override_yoast_breadcrumb_trail_two( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }else if (is_singular( 'carpeting' )) {
        
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }else if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}

add_filter( 'facetwp_indexer_row_data', function( $rows, $params ) {
    if ( 'fiber' == $params['facet']['name'] ) {
        $post_id = (int) $params['defaults']['post_id'];
        $field_value = get_post_meta( $post_id, 'fiber', true );
        $new_row = $params['defaults'];
        $Nylon = array("Anso R2x","Kashmere","Caress By Shaw R2x","Unbranded","Nylon R2x","Bellera",
                                "Endura 3","Stainmaster Deluxe","Stainmaster Premier","Wear-Dated","Endura 3 Sd",
                                "ColorStrand® Nylon","Duracolor® Premium Nylon","Pet R2x","Wear-Dated Allure",
                                "Cleartouch Platinum","Cleartouch Plat Ld","Ansoex R2x","Clrtchptnd",
                                "Stainmaster Extra Life Tactess","Cleartouch","Anso R2x (old Caress)","ColorStrand® SD Nylon",
                                "Stainmaster","Anso Rna R2x","Solutia Ultron","Wear-Dated SoftTouch","Nylon Bl Diff");
        $Wool = array("New Zealand Wool","New Zealand wool","Wool and SmartStrand Silk","Wool and Wool Blends","Wool and SmartStrand Blend");
        $Polypropylene = array("EverStrand BCF","EverStrand BCF/Triexta Blend","Smpetprtct","Permastrand","PermaStrand","Air.O",
                            "Evertouch R2x","S Safety","Stain Resist Hp","EverStrand BCF PET","EverStrand Soft Appeal",
                            "EverStrand","100% Sd Pet Polyester","UltraStrand","Solution Q","100% Pet Polyester",
                        "Ecosoltn Q","Eco Sol Q Bl Diff","Eco Sol Q Tiles Diff","Poly Tile Diff","Sol Q Bl Diff");
                            
        $SmartStrand = array("SmartStrand w/DuPont Sorona Ultra","SmartStrand Silk w/DuPont Sorona","SmartStrand",
                        "Forever Fresh Ultrasoft","SmartStrand Forever Clean Silk","SmartStrand Silk PET Blend","SmartStrand Sorona",
                        "SmartStrand PET Blend","SmartStrand Silk Reserve","SmartStrand Forever Clean Ultra",
                        "Triexta PET Blend","SmartStrand Ultra PET Blend","SmartStrand w/DuPont Sorona");                    
        
        if(in_array(trim($field_value) , $Nylon)){
            $new_row['facet_value'] = "Nylon";
            $new_row['facet_display_value'] = "Nylon"; 
         }else if(in_array(trim($field_value) , $Wool)){
            $new_row['facet_value'] = "Wool";
            $new_row['facet_display_value'] = "Wool"; 
         }else if(in_array(trim($field_value) , $Polypropylene)){
            $new_row['facet_value'] = "Polypropylene";
            $new_row['facet_display_value'] = "Polypropylene"; 
         }else if(in_array(trim($field_value) , $SmartStrand)){
            $new_row['facet_value'] = "SmartStrand";
            $new_row['facet_display_value'] = "SmartStrand"; 
         }else{
            $new_row['facet_value'] = $field_value; // value
            $new_row['facet_display_value'] = $field_value; // label
         }
         $rows =array();
        $rows[] = $new_row;
    }
    return $rows;
}, 10, 2 );


//301 redirect added from 404 logs table
wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('markville_404_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', 'markville_404_redirection_301_log_cronjob');
}

add_action( 'markville_404_redirection_301_log_cronjob', 'markville_custom_404_redirect_hook' ); 

function custom_check_404($url) {
   $headers=get_headers($url, 1);
  if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function markville_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false ) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/carpet/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false ) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/hardwood/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false ) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/laminate/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false ) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url = '/vinyl/products/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url = '/vinyl/products/';
            }
            
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false ) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url = '/tile/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }


add_filter( 'gform_pre_render_33', 'populate_product_color' );
add_filter( 'gform_pre_validation_33', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_33', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_33', 'populate_product_color' );

function populate_product_color( $form ) {
     foreach ( $form['fields'] as &$field ) {

          // Only populate field ID 12
          if( $field['id'] == 22 ) {
              
			//  $get_post_id = "965643";
              $get_post_id = $_GET['id'];

             $flooringtype = get_post_type($get_post_id); 
               $collection =  get_post_meta($get_post_id,'collection',true); 
			   $satur = array('Masland','Dixie Home');  
											
                if($collection != NULL ){
                    
                    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                        $familycolor = get_post_meta($get_post_id,'style',true); 
                        $key = 'style';

                    }else{

                        $familycolor = $collection;   
                        $key = 'collection';
                        
                    }	
                }else{	
                    
                    if (in_array(get_post_meta($get_post_id,'brand',true), $satur)){
                        $familycolor = get_post_meta($get_post_id,'design',true); 
                        $key = 'design';
                        }

                }	

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $familycolor,
                                'compare' => '='
                            ),
                            array(
                                'key' => 'swatch_image_link',
                                'value' => '',
                                'compare' => '!='
                                )
                        )
                    );										
            
                    $the_query = new WP_Query( $args );
                                            
                                            
               $choices = array(); // Set up blank array

              
                    // loop over each select item at add value/option to $choices array

                while ($the_query->have_posts()) {
                    $the_query->the_post();

                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');

                    $brand = get_post_meta(get_the_ID(),'brand',true);
                    $collection = get_post_meta(get_the_ID(),'collection',true);
                    $color = get_post_meta(get_the_ID(),'color',true);

                    $text = $brand.' '.$collection.' '.$color;
                    
                         $choices[] = array( 'text' =>  get_the_title(get_the_ID()) , 'price' =>  $image, 'value' => get_the_title(get_the_ID()) );

                }
                wp_reset_postdata();

               // Set placeholder text for dropdown
               $field->placeholder = '-- Choose color --';

               // Set choices from array of ACF values
               $field->choices = $choices;

          }
     }
return $form;
}

